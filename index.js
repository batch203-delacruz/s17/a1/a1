/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:
	function getUserInfo(){
		let fullName = prompt("Enter your full name.");
		let age = prompt("Enter your age.");
		let location = prompt("Enter your location.");
		console.log("Hello, "+fullName);
		console.log("You are "+age+ " years old.");
		console.log("You live in "+location);
	}
	getUserInfo();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:
	function showMusicalArtists(){
		let artist1 = "Avril Lavigne";
		let artist2 = "Taylor Swift";
		let artist3 = "Maroon 5";
		let artist4 = "Ben & Ben";
		let artist5 = "Ed Sheeran";
		console.log("1. "+artist1);  
		console.log("2. "+artist2);
		console.log("3. "+artist3);
		console.log("4. "+artist4);
		console.log("5. "+artist5);
		 
	}
	showMusicalArtists();
/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
	function displayMovie(){
		let movie1 = "The Lord of the Rings";
		let movie2 = "The Hobbit";
		let movie3 = "The Notebook";
		let movie4 = "The Choice";
		let movie5 = "Angels and Demons";
  		console.log("1. "+movie1);
		console.log("Rotten Tomatoes Rating: 91%");
		console.log("2. "+movie2);
		console.log("Rotten Tomatoes Rating: 64%");
		console.log("3. "+movie3);
		console.log("Rotten Tomatoes Rating: 85%");
		console.log("4. "+movie4);
		console.log("Rotten Tomatoes Rating: 61%");
		console.log("5. "+movie5);
		console.log("Rotten Tomatoes Rating: 57%"); 
	}
	displayMovie();
	

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
}

printFriends();

// console.log(friend1);
// console.log(friend2);